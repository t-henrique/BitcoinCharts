﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using BitcoinChart.Models;
using Microcharts;
using SkiaSharp;

namespace BitcoinChart.ViewModels
{
    public class BitcoinViewModel : INotifyPropertyChanged
    {
        BitcoinRequest bitcoinRequest;
        Chart lineChart;

        public event PropertyChangedEventHandler PropertyChanged;

        public BitcoinRequest BitcoinRequest 
        { 
            get
            {
                return bitcoinRequest;
            }
            set
            { 
                bitcoinRequest = value;
                OnPropertyChanged();
            } 
        }

        public Chart LineChart 
        { 
            get 
            {
                return lineChart;
            } 
            set
            {
                lineChart = value;
                OnPropertyChanged();
            }
        }

        public Chart createChart(IEnumerable<Entry> entries)
        {
            return new PointChart()
            {
                Entries = entries,
                PointMode = PointMode.Circle,
                LabelTextSize = 23,
            };
        }


        public BitcoinViewModel()
        {
            BitcoinRequest = new BitcoinRequest();
        }
        public async Task getBitcoinPriceAtPeriod(string period)
        {
            var request = new BitcoinRequest();
            var returnedData = await request.getBitcoinDataFromRepository(period);
            if(returnedData.error == null)
            {
                BitcoinRequest = returnedData.bitcoinRequest;
                LineChart = createChart(returnEntriesFormatted(returnedData.bitcoinRequest.values));
            }
        }

        private IEnumerable<Entry> returnEntriesFormatted(List<ChartCordinates> chartCordinates)
        {
            var entries = new List<Entry>();

            foreach (var item in chartCordinates)
            {
                entries.Add(
                    new Microcharts.Entry(item.y)
                    {
                        Label = chartCordinates.Count < 16 ? item.Date.ToString("dd") : "",
                        ValueLabel = item.y.ToString("0.00"),                       
                        Color = SKColor.Parse("#3cb"),
                    });
            }
            return entries;
        }


        void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
