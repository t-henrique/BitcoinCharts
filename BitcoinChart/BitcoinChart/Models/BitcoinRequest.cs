﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using BitcoinChart.Repository;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace BitcoinChart.Models
{
    public class BitcoinRequest
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string unit { get; set; } 
        public string period { get; set; } 
        public string description{ get; set; }
        [OneToMany]
        public List<ChartCordinates> values { get; set; }

        public async Task<ReturnBitcoinRequest> getBitcoinDataFromRepository(string period)
        {
            var repository = new BitcoinRepository();
            return await repository.getBitCoinData(period);
        }

        public BitcoinRequest()
        {
            
        }
    }
}
