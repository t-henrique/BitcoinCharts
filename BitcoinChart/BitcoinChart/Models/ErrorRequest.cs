﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitcoinChart.Models
{
    public class ErrorRequest
    {
        public string statusCode { get; set; }
        public string message { get; set; }
    }
}
