﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitcoinChart.Models
{
    public class ReturnBitcoinRequest
    {
        public BitcoinRequest bitcoinRequest { get; set; }
        public ErrorRequest error { get; set; }
    }
}
