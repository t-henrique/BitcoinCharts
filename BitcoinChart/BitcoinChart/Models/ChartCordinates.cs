﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace BitcoinChart.Models
{
    public class ChartCordinates
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        [ForeignKey(typeof(BitcoinRequest))]
        public int BitcoinRequestID { get; set; }
        DateTime date;
        [JsonIgnore]
        public DateTime Date 
        { 
            get
            {
                DateTime dt = new DateTime(1970, 01, 01);
                date = dt.AddSeconds(x);
                return date;
            }
            set
            {
                date = value;
            }
        }
        public long x { get; set; }
        public float y { get; set; }
    }
}
