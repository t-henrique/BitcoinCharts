﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BitcoinChart.Models;
using Newtonsoft.Json;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace BitcoinChart.Repository
{
    public class BitcoinRepository
    {
        public BitcoinRepository(){}

        static string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "ormBitcoin.db3");

        public void createBitcoinRequestTableIfNotExist()
        {
            var db = new SQLiteConnection(dbPath);
            db.CreateTable<BitcoinRequest>();
            db.CreateTable<ChartCordinates>();

        }

        public async Task<ReturnBitcoinRequest> getBitCoinData(string period)
        {
            var retBitcoinRequest = new ReturnBitcoinRequest();

            retBitcoinRequest.bitcoinRequest = await getBitcoinDataFromLocalBase(period);

            if(retBitcoinRequest.bitcoinRequest == null)
            {
                retBitcoinRequest = await getBitCoinDataFromAPI(period);
                saveBitcoinData(retBitcoinRequest.bitcoinRequest, period);
            }
            return retBitcoinRequest;
        }

        public async Task<BitcoinRequest> getBitcoinDataFromLocalBase(string period)
        {
            using (SQLiteConnection db = new SQLiteConnection(dbPath))
            {
                var dataRetrivied = db.GetAllWithChildren<BitcoinRequest>().Where(x => x.period == period).FirstOrDefault();

                if(dataRetrivied != null)
                {
                    if (dataRetrivied.values.LastOrDefault().Date.Date != DateTime.Today.AddDays(-1).Date)
                        dataRetrivied = null;
                }
                return dataRetrivied;
            }
        }

        public async Task<ReturnBitcoinRequest> getBitCoinDataFromAPI(string period)
        {
            var bitcoinRequestReturn = new ReturnBitcoinRequest();

            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = new TimeSpan(0, 0, 30);

                var uri = String.Format("https://api.blockchain.info/charts/market-price?format=json&timespan={0}", period);

                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var retorno = await response.Content.ReadAsStringAsync();

                    bitcoinRequestReturn.bitcoinRequest = JsonConvert.DeserializeObject<BitcoinRequest>(retorno);
                }
                else
                {
                    bitcoinRequestReturn.error = new ErrorRequest()
                    {
                        message = "We have some trouble. Please try again in few minutes. If error persists, contact your system administrator.",
                        statusCode = response.StatusCode.ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                bitcoinRequestReturn.error = new ErrorRequest()
                {
                    message = ex.Message,
                    statusCode = "505"
                };
            }
            return bitcoinRequestReturn;
        }

        private void saveBitcoinData(BitcoinRequest bitcoinRequest, string period)
        {
            bitcoinRequest.period = period;

            using(SQLiteConnection db = new SQLiteConnection(dbPath))
            {
                db.Insert(bitcoinRequest);
                db.InsertAll(bitcoinRequest.values);
                db.UpdateWithChildren(bitcoinRequest);
            }
        }

    }
}
