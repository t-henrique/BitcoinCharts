﻿using System;
using System.Collections.Generic;
using Android.OS;
using BitcoinChart.ViewModels;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;

namespace BitcoinChart.Droid.Views
{
    public partial class Chart : ContentPage
    {
        BitcoinViewModel bitcoinViewModel;

        public Chart()
        {
            bitcoinViewModel = new BitcoinViewModel();
            this.BindingContext = bitcoinViewModel;

            InitializeComponent();

            btnMonth.Clicked += (sender, e) => getChartsData("1months");
            btnFifteen.Clicked += (sender, e) => getChartsData("15days");
            btnWeek.Clicked += (sender, e) => getChartsData("1week");
            btnThree.Clicked += (sender, e) => getChartsData("3days");

        }

        protected override void OnAppearing()
        {
            bitcoinViewModel.getBitcoinPriceAtPeriod("1days");
        }

        private void getChartsData(string period)
        {
            bitcoinViewModel.getBitcoinPriceAtPeriod(period);
        }

    }
}
